@extends('layouts.app')
@section('content')

<form action="{{ route('supplies.store') }}" method="post">
    @csrf

    <h1 class="card-title"> {{ $supply->name }} </h1>

    <p class="card-text"> {{ $supply->price }} </p>
    <input type="number" name="supplyid" value="{{$supply->id}}" class="hidden">

    <div class="form-group">
        <label for="amount">amount</label>
        <textarea name="amount"></textarea>
    </div>

    <div class="form-group">
        <input type="submit" value="Order this product">
    </div>
</form>

@endsection
@extends('layouts.app')
@section('content')
    <div class="container">
    <form action="{{route('supplies.filter')}}" method="post">
        @csrf
        <div class="form-group">
            <input type="text" name="Name">
            <input type="submit" name="submitbtn" value="Search">
            <input type="submit" name="submitbtn" value="Clear">
        </div>
        <div class="form-group">
            <input type="radio" value="enough" name="enough"> Momenteel leverbaar
            <input type="radio" value="to-little" name="enough"> Niet meer op voorraad
        </div>
    </form>

    <div class="container-fluid">
        <div class="row">


            <div class="col-4">Naam product</div>
            <div class="col-4">Prijs</div>
            <div class="col-4">Voorraad</div>
            @foreach($supplies as $supply)
                <div class="col-4"> <a href="{{ route('supplies.show', $supply->id ) }}"> {{ $supply->name }}  </a>   </div>
                <div class="col-4">{{$supply->price}}</div>
                <div class="col-4">{{$supply->amount}}</div>
            @endforeach
        </div>

    </div>
    </div>





@endsection
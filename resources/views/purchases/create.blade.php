@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        <form action="{{route('purchases.store' )}}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-3">Naam product</div>
                <div class="col-md-3">Prijs</div>
                <div class="col-md-3">Voorraad</div>
                <div class="col-md-3">bestellen</div>
                @foreach($supplies as $supply)
                    <div class="col-md-3">{{$supply->name}}</div>
                    <div class="col-md-3">{{$supply->price}}</div>
                    <div class="col-md-3">{{$supply->amount}}</div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="hidden" name="">
                            <input type="number" name="{{$supply->id}}" id="{{$supply->id}}">
                        </div>
                    </div>
                @endforeach

                <input type="submit" class="btn btn-success">
            </div>
        </form>
    </div>
@endsection
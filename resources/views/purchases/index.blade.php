@extends('layouts.app')
@section('content')
    <div class="parallax">
        <div class="container">
    @foreach($purchases as $purchase)
        <div class="user_name">Naam van inkoopmedewerker: {{ $purchase->user->name }}</div>
        <a href="{{ route('purchases.show', $purchase) }}">Bekijik hem hier</a>
    @endforeach
        </div>
    </div>
@endsection
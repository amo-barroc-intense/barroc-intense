@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="headflex">
                <div style="margin-top: 50px;">
                    <div class="logo">
                        <div class="left_corner_logo logo_text"><p>BARROC</p></div>
                        <div class="logo_image"></div>
                        <div class="right_bottom_logo logo_text"><p>INTENSE</p></div>
                    </div>
                </div>
                <div class="listflex">
                    <h1>besteloverzicht</h1>
                    <h4>bestelnummer {{$purchase->id}}</h4>
                    <div class="productsButton">
                        <ul>
                            @foreach($purchase->supplies as $supply)
                                <li>{{$supply->name}}</li>
                                <li>{{$supply->price_per_unit}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
@endsection
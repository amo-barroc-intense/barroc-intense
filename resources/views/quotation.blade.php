@extends('layouts.app')
@section('content')
    <div class="parallax">
        <div class="central">
            <div class="content">
                <div class="form-group form">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                    <form action="{{ route('quotations.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
                        </div>

                        <div class="form-group">
                            <label for="price">price</label>
                            <input class="form-control" type="number" name="price" id="price"  value="{{ old('price') }}">
                        </div>

                        <div class="form-group">
                            <label for="price_vat">price with vat</label>
                            <input class="form-control" type="number" name="price_vat" id="price_vat" value="{{ old('price_vat') }}">
                        </div>

                        <div class="form-group">
                            <label for="bkr_check">BKR check</label>
                            <input name="bkr_check" id="bkr_check" type="checkbox" required>
                        </div>
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" value="make quotations">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                margin: 0;
            }
            p, a, label{
                color:yellow;
                text-decoration: none;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .logo{
                position: relative;
            }
            .logo_image img{
                height: auto;
                width: 400px;
            }
            .left_corner_logo, .right_bottom_logo{

                position: absolute;
                font-size: 0.5em;
                color: yellow;
            }
            .left_corner_logo{
                left: -22%;
                top: 15%;
            }
            .right_bottom_logo{
                right: -18%;
                top: 65%;
            }
            .grid{
                display: grid;
                grid-template-columns: 1fr 1fr 1fr 1fr;
                padding: 50px 0;
            }
            .grid:nth-child(2){
                padding-top: 1em;
            }
            .grid .last_child{
                grid-column-start: 4;
                grid-column-end: 5;
            }
            .grid:nth-child(3){
                paddin-top: 1em;
            }
            .grid div img{
                height: auto;
                width: 200px;
            }
            .request_quotation{
                grid-column-start: 2;
                grid-column-end: 4;
                grid-row-start: 1;
                grid-row-end: 2;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .request_quotation a{
                color: blue;
                padding: 5px;
                background-color: #ffed4a;
                border-radius: 5%;
            }
            .request_quotation a:hover{
                text-decoration-line: none;
                color:blue;
                padding: 5px;
                background-color: #fde300;
            }
            .central{
                width: 800px;
                margin: 0 auto;
                display: flex;
                justify-content: center;
                align-items: center;
                padding-top: 10%;
            }
            .form-group label{
                color:yellow;
            }
        </style>
    </head>
    <body>
    @extends('layouts.app')
    @section('content')
        <div class="parallax">
            <div class="central">
            <div class="content">
                <div class="title m-b-md">
                    <div class="logo" id="logotitle">
                        <div class="left_corner_logo"><p>BARROC</p></div>
                        <div class="logo_image"></div>
                        <div class="right_bottom_logo "><p>INTENSE</p></div>
                    </div>
                </div>

                <div class="grid">
                    <div>
                        <a href=""><img src="{{'images/bit-deluxe.png'}}" alt=""></a>
                    </div>
                    <div class="request_quotation">
                        <a href="">Offerte aanvragen</a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-light.png'}}" alt=""></a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-light.png'}}" alt=""></a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-deluxe.png'}}" alt=""></a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-light.png'}}" alt=""></a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-deluxe.png'}}" alt=""></a>
                    </div>
                    <div>
                        <a href=""><img src="{{'images/bit-light.png'}}" alt=""></a>
                    </div>
                    <div class="last_child">
                        <a href=""><img src="{{'images/bit-deluxe.png'}}" alt=""></a>
                    </div>
                </div>
                <div class="form-group form">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                    <form action="{{ route('ContactForm.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="form-group">
                            <label for="email">email</label>
                            <input class="form-control" type="text" name="email"  value="{{ old('email') }}">
                        </div>

                        <div class="form-group">
                            <label for="message">message</label>
                            <input class="form-control" type="text" name="message" id="message" >
                        </div>

                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" value="Contact Us">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
        </div>
        <script src="../js/app.js"></script>
    </body>
</html>
@endsection

@extends('layouts.app')
@section('content')
    <h1>Create work order</h1>
    <form action="{{ route('workOrder.store') }}" method="post">
        @csrf

        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description"></textarea>
        </div>

        <div class="form-group">
            <label for="lease_type_id">lease id</label>
            <textarea name="lease_type_id"></textarea>
        </div>


        <div class="form-group">
            <input type="submit" value="Create workorder">
        </div>
    </form>
@endsection

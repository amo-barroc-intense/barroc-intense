@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @auth
                        @foreach($users as $user)
                        <h3><strong>welkom, {{ $user->name }}</strong></h3>
                            @endforeach
                        <h5>your info</h5>
                        <ul>
                            @foreach ($users as $user)
                                <li>name: {{$user->name}}</li>
                                <li>email: {{$user->email}}</li>
                                @if($user->role_id == 1)
                                    <a href="{{ route('users.index', $user->role_id) }}">bewerk persoongegevens</a>

                                @endif
                                @if($user->role_id == 4)
                                    <li>positie: maintenance medewerker</li>
                                    <a href="{{ route('error.index') }}">error lijst</a>
                                @endif
                                @if($user->role_id == 7)
                                    @foreach($contractsInfo as $contractInfo)
                                    <div class="contract-info border border-info">
                                        <div class="invoices border-right">
                                            <h4>Contract Info
                                            </h4>
                                            <p>invoices nr: {{ $contractInfo->invoicesnummer }}</p>
                                            <p>contract nr: {{ $contractInfo->quotationsnummer }}</p>
                                            <p>lease nr: {{ $contractInfo->leasenummer }}</p>
                                            <p>naam: {{ $contractInfo->name }}</p>
                                            <p>prijs: {{ $contractInfo->price }}</p>
                                            @if(empty($contractInfo->betaald_op))
                                                <p>nog niet betaald</p>
                                                @else
                                            <p>betaald op: {{ $contractInfo->betaald_op }}</p>
                                                @endif
                                            <p>financiën goedkeuring: @if($contractInfo->finance_approved == 0) niet goed gekeurd @elseif($contractInfo->finance_approved == 1)
                                            goed gekeurd @endif</p>
                                            <p>klant goedkeuring: @if($contractInfo->customer_approved == 0) niet goed gekeurd @elseif($contractInfo->customer_approved == 1)
                                                    goed gekeurd @endif</p>
                                        </div>
                                    </div>
                                    @endforeach
                                    <a class="btn btn-info" href="{{ route('error.create') }}">maak storing melding</a>
                                    <a href="{{ route('users.edit', $user->role_id) }}">bewerk gegevens</a>
                                @endif
                                @if($user->role_id == 3)
                                    <li>positie: medewerker finance</li>
                                @endif
                                @if($user->role_id == 6)
                                    <li>positie: medewerker inkoop</li>
                                @endif
                                @if($user->role_id == 2)
                                    <li>positie: medewerker sales</li>
                                    <a href="{{ route('users.create', Auth::id())}}">klant aanmaken</a>

                                @endif
                            @endforeach
                        </ul>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

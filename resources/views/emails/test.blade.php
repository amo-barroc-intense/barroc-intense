@component('mail::message')

    Welkom bij Barroc intense

    U heeft een contact formulier aangemaakt met de volgende informatie

    Naam: {{$name}}
    Email: {{$email}}
    Bericht: {{$message}}
    {{ config('app.name') }}
@endcomponent

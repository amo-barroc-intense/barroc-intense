@extends('layouts.app')
@section('content')
    <h3>storing melden</h3>
    <form action="{{ route('error.store') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input class="form-control" type="text" name="name" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input class="form-control" type="text" name="description" value="{{ old('description') }}">
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Create Error-request">
        </div>
    </form>
@endsection

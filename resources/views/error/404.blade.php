@extends('layouts.app')
@section('content')
    <div class="container headflex">
        <div style="margin-top: 50px;">
            <div class="logo">
                <div class="left_corner_logo logo_text"><p>BARROC</p></div>
                <div class="logo_image"></div>
                <div class="right_bottom_logo logo_text"><p>INTENSE</p></div>
            </div>
        </div>
        <div class="align-self-center">
            <h2 style="color:yellow">Sorry, Deze pagina bestaat niet</h2>
            <a href="javascript:history.back()" class="btn btn-info">klik hier om terug te gaan</a>
        </div>
    </div>
@endsection

    @extends('layouts.app')

@section('content')

    <form action="{{ route('users.store') }}" method="post">
        @csrf

        <div class="form-group">
            <label for="Name">Name</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
        </div>

        <div class="form-group">
            <label for="Email">Email</label>
            <input class="form-control" type="email" name="email" id="email" value="{{ old('email') }}">
        </div>

        <div class="form-group">
            <label for="Role">Role</label>
            <input class="form-control" type="" name="role" id="role">
        </div>

        <div class="form-group">
            <label for="Password">Password</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ old('name') }}">
        </div>


        <div class="form-group">
            <input type="submit" value="Create User">
        </div>
    </form>

@endsection

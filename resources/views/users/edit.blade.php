@extends('layouts.app')

@section('content')
    <h1>bewerk gegevens</h1>
    <form action="{{ route('users.update', $customer->id, $request->id) }}" method="POST">
        @method('PUT')
        @csrf
        @if(Auth::id() != 1)
        <div class="form-group">
            <label for="">name</label>
            <input name="name" type="text" value="{{$customer->name}}">
            <input name="email" type="text" value="{{$customer->email}}">
        </div>
        @else
            <div class="form-group">
                <label for="">name</label>
                <input name="name" type="text" value="{{ $request->name}}">
                <input name="email" type="text" value="{{ $request->email}}">
            </div>
        @endif
        <input type="submit" value="bewerk gegevens">
    </form>
@endsection



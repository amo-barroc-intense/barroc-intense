h<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('/test', function(){
   return view('testBarrocPage');
});

Route::resource('workOrder', 'workOrderController');
Route::resource('purchases', 'PurchaseController');
Route::resource('supplies', 'SuppliesController');
Route::resource('ContactForm', 'ContactFormController');
Route::resource('error', 'ErrorController');
Route::resource('quotations', 'QuotationsController');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('users', 'usersController');
// Route::get('user.master', 'usersController');

Route::post('/supplies/filter', 'SuppliesController@filter')->name('supplies.filter');



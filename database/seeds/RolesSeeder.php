<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Roleslevel tussen de 201 en 300 zijn voor de maintenance
        //Roleslevel tussen de 301 en 400 zijn voor de inkoop
        //Roleslevel tussen de 401 en 500 zijn voor de sales
        //Roleslevel tussen de 501 en 600 zijn voor de finance
        //Roleslevel tussen de 601 en 600 zijn voor de ceo
        \DB::table('roles')->insert([
            [//1
                'name' => 'Admin'
            ],
            [//2
                'name' => 'maintenance'
            ],
            [//3
                'name' => 'klant'
            ],
            [//4
                'name' => 'finance'
            ],
            [//5
                'name' => 'inkoop'
            ],
            [//6
                'name' => 'sales'
            ],
            
        ]);
    }
}

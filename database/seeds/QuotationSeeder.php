<?php

use Illuminate\Database\Seeder;

class QuotationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $customers = \App\User::where('role_id', 7)->get();
        $sales = \App\User::where('role_id', 2)->get();

        foreach($customers as $user) {
            for($i = 0; $i < $faker->numberBetween(0, 6); $i++) {
                \DB::table('quotations')->insert([
                    [
                        'sales_id'          => $sales->random()->id,
                        'customer_id'       => $user->id,
                        'name'              => $faker->text,
                        'price'             => $faker->numerify("###"),
                        'price_vat'         => $faker->numerify("###"),
                        'finance_approved'  => $faker->boolean,
                        'customer_approved' => $faker->boolean,
                        'created_at'        => $faker->dateTime,
                        'updated_at'        => $faker->dateTime,
                    ]
                ]);
            }
        }
    }
}

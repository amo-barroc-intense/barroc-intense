<?php

use App\leaseType;
use Illuminate\Database\Seeder;

class LeaseTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $periods = [['Monthly', 1], ['Quarter', 3]];

        for ($i = 0; $i < count($periods); $i++) {
            leaseType::insert([
                'name' => $periods[$i][0],
                'period' => $periods[$i][1],
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}

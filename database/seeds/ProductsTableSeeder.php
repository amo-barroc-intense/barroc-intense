<?php

use \App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $products = [
            [
                'name' => 'Barroc Intens Italian Light',
                'ean_nr' => 'S234FREKT',
                'type' => 'machine',
                'lease_price' => 699,
                'install_price' => 289,
            ],
            [
                'name' => 'Barroc Intens Italian',
                'ean_nr' => 'S234KNDPF',
                'type' => 'machine',
                'lease_price' => 899,
                'install_price' => 289,
            ],
            [
                'name' => 'Barroc Intens Italian Deluxe',
                'ean_nr' => 'S234NNBMV',
                'type' => 'machine',
                'lease_price' => 1199,
                'install_price' => 375,
            ],
            [
                'name' => 'Barroc Intens Italian Deluxe Special',
                'ean_nr' => 'S234MMPLA',
                'type' => 'machine',
                'lease_price' => 1399,
                'install_price' => 375,
            ],
            [
                'name' => 'Espresso Beneficio',
                'ean_nr' => 'S234MMPLA',
                'type' => 'beans',
                'lease_price' => 21.60,
                'install_price' => 0,
            ],
            [
                'name' => 'Espresso Beneficio',
                'ean_nr' => 'S239MNKLL',
                'type' => 'beans',
                'lease_price' => 23.20,
                'install_price' => 0,
            ],
            [
                'name' => 'Espresso Beneficio',
                'ean_nr' => 'S239IPPSD',
                'type' => 'beans',
                'lease_price' => 20.80,
                'install_price' => 0,
            ],
            [
                'name' => 'Espresso Beneficio',
                'ean_nr' => 'S239EVVFS',
                'type' => 'beans',
                'lease_price' => 27.80,
                'install_price' => 0,
            ]
        ];

        foreach ($products as $product) {
            Product::insert([
                'name' => $product['name'],
                'lease_price' => $product['lease_price'],
                'install_price' => $product['install_price'],
                'type' => $product['type'],
                'stock' => rand(6, 10),
                'ean_nr' => $product['ean_nr'],
                'minimalstock' => rand(3,5)
            ]);
        }
    }
}

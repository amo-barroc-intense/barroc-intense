<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'admin@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 4,
                'name' => 'Joris Pulles',
                'email' => 'joris@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 2,
                'name' => 'Maarten Pulles',
                'email' => 'maarten@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 7,
                'name' => 'Ingeborg van Lier',
                'email' => 'ingeborg@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 5,
                'name' => 'Ashley van de sluis',
                'email' => 'ashley@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 6,
                'name' => 'Guillaume de Randamie',
                'email' => 'guillaum@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 5,
                'name' => 'Annemie Meijaard',
                'email' => 'annemie@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 2,
                'name' => 'John Vrees',
                'email' => 'john@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 2,
                'name' => 'Evelien Rosse',
                'email' => 'evelien@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 2,
                'name' => 'Max Roerdomp',
                'email' => 'max@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 6,
                'name' => 'Simon Nagelkerke',
                'email' => 'simon@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 6,
                'name' => 'Jummy Choi',
                'email' => 'jummy@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 5,
                'name' => 'Muhammed Demir',
                'email' => 'muhammed@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 4,
                'name' => 'Paul Machielsen',
                'email' => 'paul@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 6,
                'name' => 'Cindy Passier',
                'email' => 'cindy@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
                'role_id' => 6,
                'name' => 'Piotr Loszarowski',
                'email' => 'piotr@barroc.nl',
                'password' => Hash::make('barroc123'),
                'created_at' => now(),
                'updated_at' => now()
            ]);

        User::insert([
            'role_id' => 3,
            'name' => 'TestCustomer',
            'email' => 'customer@barroc.nl',
            'password' => Hash::make('barroc123'),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 150; $i++ )
        {
            \DB::table('users')->insert([
                'role_id'   => 7,
                'name'      =>$faker->name(),
                'email'     =>$faker->email,
                'email_verified_at' =>$faker->dateTime,
                'password'      =>$faker->password(7)
            ]);
        }
    }
}

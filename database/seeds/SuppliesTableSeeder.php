<?php

use App\Supply;
use Illuminate\Database\Seeder;

class SuppliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $products =[
            [
                'name' => 'Rubber (10 mm)',
                'price' => 0.39
            ],
            [
                'name' => 'Rubber (14 mm)',
                'price' => 0.45
            ],
            [
                'name' => 'Slang',
                'price' => 4.45
            ],
            [
                'name' => 'Voeding (elektra)',
                'price' => 68.69
            ],
            [
                'name' => 'Ontkalker',
                'price' => 4.00
            ],
            [
                'name' => 'Waterfilter',
                'price' => 299.45
            ],
            [
                'name' => 'Reservoir sensor',
                'price' => 89.99
            ],
            [
                'name' => 'Druppelstop',
                'price' => 122.43
            ],
            [
                'name' => 'Electrische pomp',
                'price' => 478.59
            ],
            [
                'name' => 'Tandwiel 110 mm',
                'price' => 5.45
            ],
            [
                'name' => 'Tandwiel 70 mm',
                'price' => 5.25
            ],
            [
                'name' => 'Maalmotor',
                'price' => 119.20
            ],
            [
                'name' => 'Zeef',
                'price' => 28.80
            ],
            [
                'name' => 'Reinigingstabletten',
                'price' => 3.45
            ],
            [
                'name' => 'Reiningsborsteltjes',
                'price' => 8.45
            ],
            [
                'name' => 'Ontkalkingspijp',
                'price' => 21.70
            ],
            ];

        foreach ($products as $product) {
            Supply::insert([
                'name' => $product['name'],
                'price' => $product['price'],
                'in_stock' => rand(1, 10)

            ]);
        }
    }
}

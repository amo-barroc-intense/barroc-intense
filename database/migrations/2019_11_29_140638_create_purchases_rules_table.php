<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_id');
            $table->unsignedBigInteger('supply_id');
            $table->unsignedBigInteger('amount');
            $table->timestamps();

            $table->foreign('purchase_id')
                ->references('id')
                ->on('purchase_rules');
            //?????? purchase_rules of purchases

            $table->foreign('supply_id')
                ->references('id')
                ->on('supplies');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases_rules');
    }
}

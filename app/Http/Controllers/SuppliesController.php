<?php

namespace App\Http\Controllers;

use App\PurchaseRule;
use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Supply;
use PharIo\Manifest\Requirement;


class SuppliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function filter(Request $request)
    {
        $btn = $_POST['submitbtn'];
        if ($btn == "clear"){
            $supplies = Supply::all();
            return view('supplies/index', ['supplies' => $supplies]);
        }

        $name = $request->input('Name');
        $supplies = Supply::where('name')
            ->orWhere('name', 'like', '%' . $name . '%')->get();

        $checkbox_stock = $request->input('enough', false);
        if ($checkbox_stock == 'to-little')
        {
            $supplies = Supply::where('amount')
                ->orwhere('amount', '<', 3)->get();
        }
        if ($checkbox_stock == 'enough')
        {
            $supplies = Supply::where('amount')
                ->orwhere('amount', '>', 3)->get();
        }

        return view('supplies/index', ['supplies' => $supplies]);
    }
    public function index()
    {
        $supplies = Supply::all();
        return view('supplies/index',['supplies'=>$supplies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purhaseId = Purchase::insertGetId([
            'user_id' => Auth::id(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        PurchaseRule::insert([
            'purchase_id' => $purhaseId,
            'amount' => $request->amount,
            'supply_id' => $request->supplyid,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('supplies.show', $request->supplyid);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\App\Supply $supply)
    {
        return view('supplies/show', ['supply' => $supply] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

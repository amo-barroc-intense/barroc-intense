<?php

namespace App\Http\Controllers;

use App\quotations;
use Illuminate\Http\Request;

class QuotationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('quotation');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        quotations::insert([

            'name'      => $request->name,
            'price'     => $request->price,
            'price_vat' => $request->price_vat
        ]);
        return redirect()->route('quotations.store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\quotations  $quotationsForm
     * @return \Illuminate\Http\Response
     */
    public function show(quotations $quotationsForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\quotations  $quotationsForm
     * @return \Illuminate\Http\Response
     */
    public function edit(quotations $quotationsForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\quotations  $quotationsForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, quotations $quotationsForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\quotations  $quotationsForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(quotations $quotationsForm)
    {
        //
    }
}

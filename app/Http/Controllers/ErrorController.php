<?php

namespace App\Http\Controllers;

use App\error;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ErrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $errors = error::all();
        return view('error.show', ['errors' => $errors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('error.create');
    }

    /*
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30',
            'description' => 'required'
        ]);
        $id = Auth::id();
        error::insert([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => $id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\error  $error
     * @return \Illuminate\Http\Response
     */
    public function show(error $error)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\error  $error
     * @return \Illuminate\Http\Response
     */
    public function edit(error $error)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\error  $error
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, error $error)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\error  $error
     * @return \Illuminate\Http\Response
     */
    public function destroy(error $error)
    {
        //
    }
}

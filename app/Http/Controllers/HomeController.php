<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::id();
        $users = \DB::select("select * from users where id = $id");
        $roles = \DB::select('select roles.name from users INNER JOIN roles ON users.role_id = roles.id');
        $errors = \DB::select("select * from errors");
            $contractsInfo = \DB::select("SELECT invoices.id AS invoicesnummer,
     quotations.id AS quotationsnummer,
     invoices.lease_id AS leasenummer,
      invoices.betaald_op,
       quotations.name,
        quotations.price,
         quotations.price_vat,
          quotations.finance_approved,
           quotations.customer_approved 
           FROM invoices 
           INNER JOIN leases ON invoices.lease_id = leases.id 
           INNER JOIN users ON leases.customer_id = users.id 
           INNER JOIN quotations ON users.id = quotations.customer_id 
       WHERE quotations.customer_id = $id");
        return view('home', ['users' => $users, 'roles' => $roles, 'errors' => $errors, 'contractsInfo' => $contractsInfo]);
       // dd($contractsInfo);
    }
}


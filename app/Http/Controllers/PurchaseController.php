<?php

namespace App\Http\Controllers;

use App\Supply;
use App\Purchase;
use App\PurchaseRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::all();
        return view('purchases.index', ['purchases' => $purchases]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplies = Supply::all();
        return view('purchases.create', ['supplies' => $supplies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $purchaseId = Purchase::insertGetId([
            'user_id' => Auth::user()->id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        foreach($request->except('_token') as $amount ){
            if ($amount != null) {
                PurchaseRule::insert([
                    'amount' => $amount,
                    'supply_id' =>$request->supplyid,
                    'purchase_id' => $purchaseId,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
        return view('purchases.index');
        return redirect()->route('purchases.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('purchases.show', ['purchase' => Purchase::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}

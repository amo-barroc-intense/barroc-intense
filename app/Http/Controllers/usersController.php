<?php

namespace App\Http\Controllers;

use App;
use \App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PharIo\Manifest\Email;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $errors = \DB::select("SELECT * FROM errors");
        $contractsInfo = \DB::select("SELECT invoices.id AS invoicesnummer,
     quotations.id AS quotationsnummer,
     invoices.lease_id AS leasenummer,
      invoices.betaald_op,
       quotations.name,
        quotations.price,
         quotations.price_vat,
          quotations.finance_approved,
           quotations.customer_approved 
           FROM invoices 
           INNER JOIN leases ON invoices.lease_id = leases.id 
           INNER JOIN users ON leases.customer_id = users.id 
           INNER JOIN quotations ON users.id = quotations.customer_id 
       WHERE quotations.customer_id = $id");
        $users = User::where('role_id', 7)->get();
        if($id != 1){return view('home', ['users' => $users, 'contractsInfo' => $contractsInfo, 'errors' => $errors]);}
        else {
            return view('users.index', ['users' => $users]);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        $id = Auth::id();
        $users = \DB::select("select * from users");
        return view('users.create', ['users' => $users]);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        user::insert([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => $request->role,
            'password' => $request->Password
        ]);

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editingthe specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::id();
        $customer = User::find($user);
        return view('users.edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        return redirect()->route('users.index', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

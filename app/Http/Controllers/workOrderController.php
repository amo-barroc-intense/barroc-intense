<?php

namespace App\Http\Controllers;

use App\workOrder;
use App\leaseType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class workOrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function create()
    {
        return view('workOrders/create');
    }

    public function store(Request $request)
    {


        $this->validate($request, [
            'description' => 'required|max:1000',
        ]);


        workOrder::insert([
            'description' => $request->description,
            'user_id' => Auth::id(),
            'lease_id' => $request->lease_type_id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->route('home');
    }


    public function show(\App $work_order)
    {
        return view('workOrder/show', ['work_order' => $work_order] );
    }

    public function edit(\App $work_order)
    {
        return view('workOrder.edit', [
            'description'       => description,
            'created_at'        => created_at,
            'updated_at'        => updated_at,
            'user_id'           => user_id,
            'lease_id'          => lease_id
        ]);
    }

    public function update(Request $request, $id)
    {
        $workOrder = workOrder::find($id);

        $workOrder->update([
            'description'           => $request->description,
            'created_at'          => $request->created_at,
            'updated_at'  => $request->updated_at,
            'user_id' => $request->user_id,
            'lease_id' => $request->lease_id
        ]);

        return redirect()->route('workOrder.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        workOrder::destroy($id);

        return redirect()->route('workOrders.index');
    }
}

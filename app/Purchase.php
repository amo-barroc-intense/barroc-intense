<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function supplies(){
        return $this->hasMany('\App\Supply',
                                    'id',
                                    'purchase_id');
    }
    public function user(){
        return $this->hasOne('\App\User', 'id', 'user_id');
    }
}


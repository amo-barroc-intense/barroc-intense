<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class quotations extends Model
{
    protected $fillable = ['name', 'price', 'price-vat'];
}
